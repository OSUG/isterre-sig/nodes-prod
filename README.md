## Summary:

The program **deploy2stationXML.py** builds a stationXML for dense array deployments.

### Requirements:

* python3
* obspy
* os  
* sys  
* docopt 
 

Input csv file (station file) with ";" delimiter with header fields as follow (at least):  
* "Station Name"  
* "Network Code"  
* "Latitude"  
* "Longitude"  
* "Altitude (m)"  
* "StartTime UTC (YYYY-MM-DDTHH:MM:SS)"  
* "EndTime UTC (YYYY-MM-DDTHH:MM:SS)"  
* "Sensor Type"  
* "Site Description"  

Sensor Type example :
* GSB-3C-GEOSPACE = Geospace 3C GSB nodes, 
* ZLAND-3C-FAIRFIELD = Fairfield ZLAND 3C nodes, 
* SMARTSOLO-IGU-16HR-3C = DTCC 3C nodes


### Example usage

```
python3 deploy2stationXML.py project_name station_file response_file

```

```
python3 deploy2stationXML.py FAULTSCAN metadata_Faultscan.csv GEOSPACE/ads1284-250-linear_gsb-mV-Faultscan.yaml --orientation=real

```

* station_file (csv) is stored in project_name/DEPLOY directory
* outputs are in project_name/OUTPUT directory
* --orientation option is related to Z component dip information. "real" when towards the sensor real orientation (+90° for nodes). "lb" when Z component has been inverted to follow the broadband sensors convention (-90°).

### Details

gain(dB) -> gain factor
* 0  dB -> 1
* 12 dB -> 4
* 24 dB -> 16
* 36 dB -> 64

* Gesospace nodes: data are in mV already corrected from the datalogger response and the preamp gain as well

* Fairfield and DTCC nodes: data are in mV already corrected from the datalogger response but not from the preamp gain 

* ADC SINC IIR filter is not described in the response excepted in the default [EOST] directory. Initial frequency is 1024000.0 Hz
Decimation factors are as follow: 
* 8: 4kHz
* 16: 2kHz
* 32: 1kHz
* 64: 500Hz
* 128: 250Hz


