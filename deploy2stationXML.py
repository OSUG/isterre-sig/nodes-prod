#!/Users/aubercor/anaconda3/envs/mypythonfcnt/bin/python
# -*- coding: utf-8 -*-

"""

:author:
    Coralie Aubert (coralie.aubert@univ-grenoble-alpes.fr)
    Program largely inspired from deploy2dataless.py from Maxime Bes de Berc (mbesdeberc@unistra.fr)
    Adapted for ZLAND Nodes (Fairfield) and GSB nodes (Geospace)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)


Usage:
   deploy2stationXML.py <project> <station_file> <response> \
[--network=<net>] [--station-info=<sinfo>] [--dgps-file=<dgps>] [--epsg=<epsg>] \
[--starttime=<st>] [--endtime=<et>] [--orientation=<or>] [--root=<root>] [--resp_path=<rp>]

Options:
    -h --help           Show this screen.
    --version           Show version.
    <project>           Project name.
    <station_file>      station file name
    <response>          Path to response yaml file. 
    --endtime=<et>      Give custom endtime. Endtime of the deployment YYYY-MM-DDThh:mm
    --orientation=<or>  [default: lb] or real orientation (real = Z channel is reversed compared to broadband sensors) .
    --root=<root>       Root path of GSB products \
[default: /usr/contrib/geobs/NODES].
    --resp_path=<rp>    Path of yaml response directory \
[default: /usr/contrib/geobs/NODES/cmp_response_stages].
"""

# Sensor Type example (input csv file) :
# GSB-3C-GEOSPACE = Geospace 3C GSB nodes, 
# ZLAND-3C-FAIRFIELD = Fairfield ZLAND 3C nodes, 
# SMARTSOLO-IGU-16HR-3C = DTCC 3C nodes

import obspy
from obspy.core.inventory import Inventory, Network, Station, Channel, Site
from obspy.clients.nrl import NRL
import os
import sys
from docopt import docopt
from cmp_response_stages.cmp_response import cmp_response

from utils import *


if __name__ == "__main__":
    args = docopt(__doc__, version='deploy2stationXML 1.1')
    #root_path="/Users/aubercor/Documents/PROCESSING/STATIONXML-BUILD/GSB-prod"
    workpath = "%s/%s" % (args['--root'], args['<project>'])
    deploy_dir = "%s/DEPLOY" % workpath
    output_dir = "%s/OUTPUT" % workpath
    station_file = "%s/%s" % (deploy_dir, args['<station_file>'])
    resp_file = "%s/%s" % (args['--resp_path'], args['<response>'])

    # check if project path does exist
    if not os.path.isdir("%s/%s" % (args['--root'], args['<project>'])):
        print("[Error] Such a project does not exist, please check.")
        sys.exit()

     # create output dir if it does not exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    deploy_list=read_station_info(station_file)

    if args['--endtime']:
        print()
        print("Update w. custom endtime")
        for item in deploy_list:
            item['endtime'] = UTCDateTime(args['--endtime'])

    # write deploy in kml
    print()
    kml_path = "%s/deploy.kml" % output_dir
    deploy_2_kml(deploy_list, kml_path)

    # write deploy in fdsn xml
    fdsnxml_path = "%s/station.xml" % output_dir
    response = cmp_response(resp_file)
    print(response)
    #response.plot(0.001, output="VEL") 
    deploy_2_fdsnxml(deploy_list, response, fdsnxml_path,
                    args['--orientation'])

