# -*- coding: utf-8 -*-

import csv
from obspy import UTCDateTime
import simplekml
from obspy.core.inventory.channel import Channel
from obspy.core.inventory.inventory import Inventory
from obspy.core.inventory.network import Network
from obspy.core.inventory.station import Station
from obspy.core.inventory.util import Latitude, Longitude, Equipment, Site
from obspy import read_inventory


def deploy_2_kml(deploy_list, kml_path):
    kml = simplekml.Kml()
    print("Writing kml")
    for item in deploy_list:
        kml.newpoint(name="%s" % item['station'],
                     coords=[(item['longitude'], item['latitude'])],
                     description=item['serial'])
    kml.save(kml_path)


def deploy_2_fdsnxml(deploy_list, response, xml_path, orientation):
    print("Writing fdsnxml")
    deploy_time = min([d['starttime'] for d in deploy_list])
    pickup_time = max([d['endtime'] for d in deploy_list])
    net_code = deploy_list[0]['net']
    #for item in deploy_list:
    #    if 'endtime' in item.keys():
    #        pickup_time = max(item['endtime'], pickup_time)
    #if pickup_time.ns == 0:
    #    pickup_time = None

    # create dummy station and channel
    _dummy_sta = Station(code='DUMMY',
                         latitude=0, longitude=0, elevation=0, site=Site(name="No Description"),
                         start_date=deploy_time, end_date=pickup_time,
                         creation_date=deploy_time,
                         total_number_of_channels=3)
    sr = response.get_sampling_rates()
    sr = sr[max(sr.keys())]['output_sampling_rate']
    _dummy_chan = Channel(code='DP1', location_code='00',
                          latitude=0, longitude=0, elevation=0, depth=0,
                          azimuth=0, dip=0, sample_rate=sr,
                          start_date=deploy_time, end_date=pickup_time,
                          types=['CONTINUOUS', 'GEOPHYSICAL'], sensor=Equipment(description="No Description"),
                          response=response)

    # create network and add stations and channels
    net = Network(code=net_code, total_number_of_stations=len(deploy_list),
                  start_date=deploy_time, end_date=pickup_time)
    for item in deploy_list:
        _sta = _dummy_sta.copy()
        #_sta.code = "%s" % str(int(item['line'])*10000+int(item['station']))
        _sta.code = "%s" % item['station_code']
        _sta.latitude = Latitude(item['latitude'], datum='WGS84')
        _sta.longitude = Longitude(item['longitude'], datum='WGS84')
        if 'elevation' in item.keys():
            _sta.elevation = item['elevation']
        _sta.start_date = item['starttime']
        if 'endtime' in item.keys():
            _sta.end_date = item['endtime']
        if 'site' in item.keys():
            _sta.site = Site(name=item['site'])
        for comp in ['Z', 'N', 'E']:
            _chan = _dummy_chan.copy()
            _chan.code = _chan.code.replace('1', comp)
            _chan.latitude = _sta.latitude
            _chan.longitude = _sta.longitude
            _chan.elevation = _sta.elevation
            _chan.start_date = _sta.start_date
            _chan.end_date = _sta.end_date
            _chan.sensor = Equipment(description=item['sensor type'])
            if orientation == 'real':
                if comp == 'Z':
                    _chan.dip = 90
                elif comp == 'E':
                    _chan.azimuth = 90
            elif orientation == 'lb':
                if comp == 'Z':
                    _chan.dip = -90
                elif comp == 'E':
                    _chan.azimuth = 90
            _sta.channels.append(_chan)
        net.stations.append(_sta)

    # create and write inventory in stationxml format
    inv = Inventory(networks=[net, ])
    inv.write(xml_path, "STATIONXML", validate=True)
    inv_test = read_inventory(xml_path)
    resp_test = inv_test[0][0][0].response
    #print(resp_test)
    #resp_test.plot(0.001, output="VEL")

# def read_station_info_lhqc(station_info_file):
#     print("Reading station info file GSB-3-GEOSPACE")
#     pos_list = list()
#     with open(station_info_file) as csvfile:
#         csvreader = csv.reader(csvfile, delimiter=';')
#         header=next(csvreader)
#         for row in csvreader:
#             #print(row)
#             pos = dict()
#             pos['serial'] = row[1]
#             pos['line'] = row[3]
#             pos['station'] = row[4]
#             pos['station_code'] = str(int(pos['line'])*10000+int(pos['station'])) # station code = line number + station number over 5 digits
#             pos['latitude'] = row[42]
#             pos['longitude'] = row[43]
#             pos['starttime'] = UTCDateTime.strptime(row[9], '%m/%d/%Y %H:%M')       
#             pos_list.append(pos)
#         #print(pos_list)
#     return pos_list


def read_station_info(station_info_file):
    print("Reading station info file from a csv file")
    pos_list = list()
    with open(station_info_file) as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=';')
        #header=next(csvreader)
        for row in csvreader:
            #print(row)
            pos = dict()
            pos['serial'] = row['Station Name']
            #pos['line'] = row[2]
            pos['station'] = row['Station Name']
            pos['station_code'] = row['Station Name'] # station code = station
            pos['net'] = row['Network Code']
            pos['latitude'] = row['Latitude']
            pos['longitude'] = row['Longitude']
            pos['elevation'] = row['Altitude (m)']
            pos['starttime'] = UTCDateTime.strptime(row['StartTime UTC (YYYY-MM-DDTHH:MM:SS)'], '%Y-%m-%dT%H:%M:%S')
            pos['endtime'] = UTCDateTime.strptime(row['EndTime UTC (YYYY-MM-DDTHH:MM:SS)'], '%Y-%m-%dT%H:%M:%S') 
            pos['sensor type'] = row['Sensor Type']
            pos['site'] = row['Site Description']      
            pos_list.append(pos)
        #print(pos_list)
    return pos_list
